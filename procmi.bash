#!/bin/bash

for arg
do
	filnavn=$arg-$(date +%Y%m%d-%H:%M:%S).meminfo
	vmData=$(grep VmData /proc/"$arg"/status | awk '{print $2}') # privat memory
	vmStk=$(grep VmStk /proc/"$arg"/status | awk '{print $2}')
	vmExe=$(grep VmExe /proc/"$arg"/status | awk '{print $2}')
	{
	echo "******** Minne info om prosess med PID $arg ********"
	echo "Totalt bruk av virtuelt minne (Vmsize): $(grep VmSize  /proc/"$arg"/status | awk '{print $2}') KB"
	echo "Totalt privat virtuelt size (VMDATA+VMSTK+VMEXE):  (($vmData+$vmStk+$vmExe)) KB"
	echo "Mengde shared virtuelt minne (VmLib): $(grep VmLib /proc/"$arg"/status | awk '{print $2}') KB"
	echo "Total bruk av fysisk minne (VmRSS): $(grep VmRSS /proc/"$arg"/status | awk '{print $2}') KB"
	echo "Mengde fysisk minne som benyttes til page table (VmPTE): $(grep VmPTE /proc/"$arg"/status | awk '{print $2}') KB"
} >> "$filnavn"

done
