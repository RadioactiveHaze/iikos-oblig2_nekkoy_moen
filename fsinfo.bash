#!/bin/bash

brukt=$(df -k "$1" | awk '{print $5}' | tail -1)
part=$(df -k "$1" | awk '{print $1}' | tail -1)
echo "Partisjonen $part er $brukt full."

antfiler=$(find "$1" -type f 2>&1 | wc -l)

echo "Det finnes $antfiler filer."

gjsnitt=$(find "$1" -ls 2>&1| awk '{sum += $7; n++;} END {print sum/n;}')

echo "Gjennomsnittlig filstørrelse er ca $gjsnitt bytes."

storste=$(find "$1" -printf '%s\n' 2>&1|sort -nr|head -1)
path=$(find "$1" -printf '%s %p\n' 2>&1 |sort -nr|head -1 | awk '{print $2}')

echo "Den største er $path som er $storste bytes stor."

hardlinks=$(find "$1" -type f -printf '%n %p\n' 2>&1| sort -nr | head -1 | awk '{print $1}')
hardpath=$(find "$1" -type f -printf '%n %p\n' 2>&1| sort -nr | head -1 | awk '{print $2}')

echo "Filen $hardpath har flest hardlinks, den har $hardlinks."






