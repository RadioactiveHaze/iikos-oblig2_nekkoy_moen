#!/bin/bash
echo "1 - Hvem er jeg og hva er navnet på dette scriptet?"
echo "2 - Hvor lenge er det siden siste boot?"
echo "3 - Hva var gjennomsnittlig load siste minutt?"
echo "4 - Hvor mange prosesser og tråder finnes?"
echo "5 - Hvor mange context switch'er fant sted siste sekund?"
echo "6 - Hvor mange interrupts fant sted siste sekund?"
echo "9 - Avslutt dette scriptet"

read -r -p "Velg en funksjon: " numb
case $numb in
	1)
		echo "Dette er: $(whoami)"
		echo "Scriptet heter: $0"
		;;
	2)
		echo "Maskinen har vært oppe i $(uptime | awk '{print $3}')"
		;;
	3)
		echo "gjennomsnittlig load siste minutt $(uptime | awk '{print $10}')"
		;;
	4)
		f=$(cat /proc/*/status | grep Thread | awk '{ num_t += $2 } END { print num_t-2 }')
		echo "antall prosesser $(ps aux | wc -l)"
		echo "antall traader $f"
		;;
	5)
		c1=$(grep ctxt /proc/stat | awk '{print $2}')
		sleep 1
		c2=$(grep ctxt /proc/stat | awk '{print $2}')
		echo "Antall context switch'er siste sekund var $c2-$c1"
		;;
	6)
		c1=$(grep intr /proc/stat | awk '{print $2}')
                sleep 1
                c2=$(grep intr /proc/stat | awk '{print $2}')
                echo "Antall interrupts siste sekund var $c2-$c1"
                ;;
esac


