#!/bin/bash

prosess='chrome'
pids=$(pgrep $prosess)
for pid in $pids
do
	mf=$(ps --no-headers -o maj_flt "$pid")
	if [ "$mf" -gt 1000 ]
	then
		echo "Chrome $pid har foraasaket $mf major page faults (mer enn 1000!)"
	else
		echo "Chrome $pid har foraasaket $mf major page faults"
	fi
done